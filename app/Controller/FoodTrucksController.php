<?php
App::uses('AppController', 'Controller');
/**
 * FoodTrucks Controller
 *
 * @property FoodTruck $FoodTruck
 */
class FoodTrucksController extends AppController {






/**
	USE WITH CAUTION _ DB INTENSIVE
	* Update all previous pinnings for this food truck, adjusting zorder field as appropriate
	* will use model's getDataSource() if no db conneciton provdied
	*/
	
	function rankPreviousPinnings($id){
		
		$this->FoodTruck->id = $id;
		if (!$this->FoodTruck->exists()) {
			throw new NotFoundException(__('Invalid food truck'));
		}
		$this->FoodTruck->read(null,$id);
		$pinnings = $this->sortPinnings($this->FoodTruck->data['Pinning'],'modified',false);
		
		//now with ordered list, update zorder and save
		$zorderNum=1;
		$messages="starting";
		foreach ($pinnings as $pinning ){
			$this->FoodTruck->Pinning->read(null,$pinning['id']);	
			$this->FoodTruck->Pinning->data['Pinning']['zorder'] = $zorderNum;
			$quality=$this->FoodTruck->Pinning->Quality->find('first',array('conditions'=> array('Quality.zorder_low <='=>$zorderNum , 'Quality.zorder_high >='=>$zorderNum )));
			
			$this->FoodTruck->Pinning->data['Pinning']['quality_id'] =  $quality['Quality']['id']  ;
			$res=$this->FoodTruck->Pinning->save();
			$messages .= "Pinning:" . $pinning['id'] . ($res?" was successful with Rank: " . $zorderNum . "!,    ":" failed!,    ");
			if(!$res) break;
			$zorderNum++;
		}
		if($res){
			$this->Session->setFlash(__('Pinnings Have been Ranked!' . $messages));
			$this->redirect(array('action' => 'view',$id));
		}else{
			$this->Session->setFlash(__('Unable to update pinnings, see logs.' . $messages));
			$this->redirect(array('action' => 'view',$id));
		}
		
	}

	
	/** by default sort zorder descending, which gives the BEST to be FIRST
	*/
	function sortPinnings($pinnings,$column='zorder',$ascending=true){
			//pull the column we will use for an index into its own array
			foreach ($pinnings as $pinning ){
				$sortColumn[] = $pinning[$column];		
			}
			//sort pinnings based on that column
			array_multisort($sortColumn, ($ascending?SORT_ASC:SORT_DESC),  $pinnings);			
			return $pinnings;
	}


















/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->FoodTruck->recursive = 0;
		$this->set('foodTrucks', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->FoodTruck->id = $id;
		
		$this->FoodTruck->recursive = 2;
		if (!$this->FoodTruck->exists()) {
			throw new NotFoundException(__('Invalid food truck'));
		}
		$this->FoodTruck->read(null, $id);
		
		$this->set('foodTruck',$this->FoodTruck->data );
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->FoodTruck->create();
			if ($this->FoodTruck->save($this->request->data)) {
				$this->Session->setFlash(__('The food truck has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The food truck could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->FoodTruck->id = $id;
		if (!$this->FoodTruck->exists()) {
			throw new NotFoundException(__('Invalid food truck'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->FoodTruck->save($this->request->data)) {
				$this->Session->setFlash(__('The food truck has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The food truck could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->FoodTruck->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->FoodTruck->id = $id;
		if (!$this->FoodTruck->exists()) {
			throw new NotFoundException(__('Invalid food truck'));
		}
		if ($this->FoodTruck->delete()) {
			$this->Session->setFlash(__('Food truck deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Food truck was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
/**
 * administrator_index method
 *
 * @return void
 */
	public function administrator_index() {
		$this->FoodTruck->recursive = 0;
		$this->set('foodTrucks', $this->paginate());
	}

/**
 * administrator_view method
 *
 * @param string $id
 * @return void
 */
	public function administrator_view($id = null) {
		$this->FoodTruck->id = $id;
		if (!$this->FoodTruck->exists()) {
			throw new NotFoundException(__('Invalid food truck'));
		}
		$this->set('foodTruck', $this->FoodTruck->read(null, $id));
	}

/**
 * administrator_add method
 *
 * @return void
 */
	public function administrator_add() {
		if ($this->request->is('post')) {
			$this->FoodTruck->create();
			if ($this->FoodTruck->save($this->request->data)) {
				$this->Session->setFlash(__('The food truck has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The food truck could not be saved. Please, try again.'));
			}
		}
	}

/**
 * administrator_edit method
 *
 * @param string $id
 * @return void
 */
	public function administrator_edit($id = null) {
		$this->FoodTruck->id = $id;
		if (!$this->FoodTruck->exists()) {
			throw new NotFoundException(__('Invalid food truck'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->FoodTruck->save($this->request->data)) {
				$this->Session->setFlash(__('The food truck has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The food truck could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->FoodTruck->read(null, $id);
		}
	}

/**
 * administrator_delete method
 *
 * @param string $id
 * @return void
 */
	public function administrator_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->FoodTruck->id = $id;
		if (!$this->FoodTruck->exists()) {
			throw new NotFoundException(__('Invalid food truck'));
		}
		if ($this->FoodTruck->delete()) {
			$this->Session->setFlash(__('Food truck deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Food truck was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
