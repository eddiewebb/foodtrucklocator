<?php
App::uses('AppController', 'Controller');
/**
 * Qualityes Controller
 *
 * @property Quality $Quality
 */
class QualitiesController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Quality->recursive = 0;
		$this->set('statuses', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Quality->id = $id;
		if (!$this->Quality->exists()) {
			throw new NotFoundException(__('Invalid status'));
		}
		$this->set('status', $this->Quality->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Quality->create();
			if ($this->Quality->save($this->request->data)) {
				$this->Session->setFlash(__('The status has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The status could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Quality->id = $id;
		if (!$this->Quality->exists()) {
			throw new NotFoundException(__('Invalid status'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Quality->save($this->request->data)) {
				$this->Session->setFlash(__('The status has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The status could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Quality->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Quality->id = $id;
		if (!$this->Quality->exists()) {
			throw new NotFoundException(__('Invalid status'));
		}
		if ($this->Quality->delete()) {
			$this->Session->setFlash(__('Quality deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Quality was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
