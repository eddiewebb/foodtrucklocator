<?php
App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');
/**
 * Pinnings Controller
 *
 * @property Pinning $Pinning
 */
class PinningsController extends AppController {

	public function isAuthorized($user) {
		// All registered users can add posts
		if ($this->action === 'add') {
			return true;
		}	

		return parent::isAuthorized($user);
	}
/**
 * index method
 *
 * @return void
 */
	public function index() {
		if($this->Session->check('User.centroid')){
				$this->request->query['centroid'] = $this->Session->read('User.centroid');
		}else if(!isset($this->request->query['centroid'])){
				$this->redirect(array('action' => 'findme'));
		}else{
			$this->Session->write('User.centroid',$this->request->query['centroid']);
		}
		
		if(!isset($this->request->query['radius'])){
			$this->request->query['radius'] = 20;
		}
		
		$this->set('pinnings', $this->Pinning->withinRadiusOfCentroid($this->request->query['radius'],$this->request->query['centroid']));
		$this->set('centroid', Sanitize::paranoid($this->request->query['centroid'], array(' ', ',','-','.')));
		$this->set('radius', Sanitize::paranoid($this->request->query['radius']));
		
		
		$foodTrucks = $this->Pinning->FoodTruck->find('list');
		$this->set(compact('foodTrucks'));
		
	}
	
	
/**
 * findme method (locates user via html5 or zipcoede_
 *
 * @return void
 */
	public function findme() {
	
		$this->Session->setFlash('In order to locate nearby trucks we need your location, Please allow the pending location request or provide your local zip code');
				
		
	}

	
	

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Pinning->id = $id;
		if (!$this->Pinning->exists()) {
			throw new NotFoundException(__('Invalid pinning'));
		}
		$this->set('pinning', $this->Pinning->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Pinning->create();
			$this->request->data['Pinning']['user_id'] = $this->Auth->user('id'); //Added this line
			if ($this->Pinning->save($this->request->data)) {
				$this->Session->setFlash(__('The pinning has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The pinning could not be saved. Please, try again.'));
			}
		}
		$foodTrucks = $this->Pinning->FoodTruck->find('list');
		$users = $this->Pinning->User->find('list');
		$statuses = $this->Pinning->Quality->find('list');
		$this->set(compact('foodTrucks', 'users', 'statuses'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Pinning->id = $id;
		if (!$this->Pinning->exists()) {
			throw new NotFoundException(__('Invalid pinning'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Pinning->save($this->request->data)) {
				$this->Session->setFlash(__('The pinning has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The pinning could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Pinning->read(null, $id);
		}
		$foodTrucks = $this->Pinning->FoodTruck->find('list');
		$users = $this->Pinning->User->find('list');
		$statuses = $this->Pinning->Quality->find('list');
		$this->set(compact('foodTrucks', 'users', 'statuses'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Pinning->id = $id;
		if (!$this->Pinning->exists()) {
			throw new NotFoundException(__('Invalid pinning'));
		}
		if ($this->Pinning->delete()) {
			$this->Session->setFlash(__('Pinning deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Pinning was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
