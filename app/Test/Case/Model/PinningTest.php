<?php
App::uses('Pinning', 'Model');

/**
 * Pinning Test Case
 *
 */
class PinningTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.pinning');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Pinning = ClassRegistry::init('Pinning');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Pinning);

		parent::tearDown();
	}

}
