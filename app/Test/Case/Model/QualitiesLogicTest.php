<?php
App::uses('QualitiesLogic', 'Model');

/**
 * QualitiesLogic Test Case
 *
 */
class QualitiesLogicTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.qualities_logic', 'app.role', 'app.user', 'app.pinning', 'app.food_truck', 'app.status', 'app.quality');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->QualitiesLogic = ClassRegistry::init('QualitiesLogic');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->QualitiesLogic);

		parent::tearDown();
	}

}
