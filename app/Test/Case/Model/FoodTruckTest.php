<?php
App::uses('FoodTruck', 'Model');

/**
 * FoodTruck Test Case
 *
 */
class FoodTruckTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.food_truck', 'app.pinning');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->FoodTruck = ClassRegistry::init('FoodTruck');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->FoodTruck);

		parent::tearDown();
	}

}
