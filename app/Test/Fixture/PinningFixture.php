<?php
/**
 * PinningFixture
 *
 */
class PinningFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 20, 'key' => 'primary'),
		'food_truck_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 20),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 20, 'key' => 'index'),
		'modified' => array('type' => 'timestamp', 'null' => false, 'default' => 'CURRENT_TIMESTAMP'),
		'status_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 20),
		'lat_long' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 100, 'key' => 'index', 'collate' => 'utf8_bin', 'charset' => 'utf8'),
		'ftl_gps_point' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'index'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'user_id' => array('column' => 'user_id', 'unique' => 0), 'point' => array('column' => 'lat_long', 'unique' => 0), 'ftl_gps_point' => array('column' => 'ftl_gps_point', 'unique' => 0)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_bin', 'engine' => 'MyISAM')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'food_truck_id' => 1,
			'user_id' => 1,
			'modified' => 1334064064,
			'status_id' => 1,
			'lat_long' => 'Lorem ipsum dolor sit amet',
			'ftl_gps_point' => 1
		),
	);
}
