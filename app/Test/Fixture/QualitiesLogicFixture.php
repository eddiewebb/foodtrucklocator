<?php
/**
 * QualitiesLogicFixture
 *
 */
class QualitiesLogicFixture extends CakeTestFixture {
/**
 * Table name
 *
 * @var string
 */
	public $table = 'qualities_logic';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'role_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 20, 'key' => 'primary', 'comment' => 'i.e "user_id","created<="'),
		'days_old' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary', 'comment' => '"36","2012-01-01 HH:MM:SS"'),
		'quality_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 20),
		'indexes' => array('PRIMARY' => array('column' => array('role_id', 'days_old'), 'unique' => 1)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_bin', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'role_id' => 1,
			'days_old' => 1,
			'quality_id' => 1
		),
	);
}
