<div class="users view">
<h2><?php  echo __('User');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Username'); ?></dt>
		<dd>
			<?php echo h($user['User']['username']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Pinnings'), array('controller' => 'pinnings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pinning'), array('controller' => 'pinnings', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Pinnings');?></h3>
	<?php if (!empty($user['Pinning'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Food Truck Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Status Id'); ?></th>
		<th><?php echo __('Point'); ?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($user['Pinning'] as $pinning): ?>
		<tr>
			<td><?php echo $pinning['id'];?></td>
			<td><?php echo $pinning['food_truck_id'];?></td>
			<td><?php echo $pinning['user_id'];?></td>
			<td><?php echo $pinning['modified'];?></td>
			<td><?php echo $pinning['status_id'];?></td>
			<td><?php echo $pinning['point'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'pinnings', 'action' => 'view', $pinning['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'pinnings', 'action' => 'edit', $pinning['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'pinnings', 'action' => 'delete', $pinning['id']), null, __('Are you sure you want to delete # %s?', $pinning['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Pinning'), array('controller' => 'pinnings', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
