




<div id="map_canvas" style="width:100%; height:400px"></div>

<?php
function zoomLevel ($distance){
    $zoom=1;
    $ee = 21638.8;
    $zoom = round(log($ee/$distance)/log(2)+1);
    // to avoid exeptions
    if ($zoom>21) $zoom=21;
    if ($zoom<1) $zoom =1;

    return $zoom;
}
?>



<script type="text/javascript">
	function initialize() {
	
		var myOptions = {
		  zoom: <?php echo zoomlevel($radius); ?>,
		  center: new google.maps.LatLng(<?php echo $centroid; ?>),
		  mapTypeId: google.maps.MapTypeId.ROADMAP
		};

	   var map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
		
		// Marking targets now Sir...
		<?php 
		foreach ($pinnings as $pinning):  ?>
		
			var image = new google.maps.MarkerImage(
				'<?php echo $this->webroot."/img/".$pinning['FoodTruck']['image']; ?>',
				// This marker is 20 pixels wide by 32 pixels tall.
				new google.maps.Size(24, 24));
		
		
			   marker = new google.maps.Marker({
					animation: google.maps.Animation.DROP,	
					icon: image,			
					position: new google.maps.LatLng(<?php echo h($pinning['Pinning']['lat_long']); ?>),
					title:'<?php echo $pinning['FoodTruck']['name']; ?>'
				});
				// To add the marker to the map, call setMap();
				marker.setMap(map);		
		<?php endforeach; ?>	
	
	}
		
		
		
		

		
</script>







<div style"display:hidden;margin:auto; width:400px;height:300px;border:3px solid blue;">
<?php
    echo $this->Form->create('Pinning');
    echo $this->Form->input('food_truck_id');
    echo $this->Form->end();
?>
</div>




<div class="pinnings index">
<form>
	<h2>FoodTrucks within <input name="radius" style="clear:none;font-size:100%;line-height:0;width:40px;padding:0;" size="4" value="<?php echo $radius?>" /> miles of <?php echo $centroid ?></h2>
	<input type="hidden" name="centroid" value="<?php echo $centroid;?>" />
</form>
	<table cellpadding="0" cellspacing="0">

	<?php
foreach ($pinnings as $pinning): ?>
	<tr>
		<td><?php echo $this->Html->image($pinning['FoodTruck']['image'], array('alt' => $pinning['FoodTruck']['name']))?> 
</td>
		<td>
			<?php echo $this->Html->link($pinning['FoodTruck']['name'], array('controller' => 'food_trucks', 'action' => 'view', $pinning['FoodTruck']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($pinning['User']['username'], array('controller' => 'users', 'action' => 'view', $pinning['User']['id'])); ?>
		</td>
		<td><?php echo h($pinning['Pinning']['modified']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($pinning['Quality']['title'], array('controller' => 'qualities', 'action' => 'view', $pinning['Quality']['id'])); ?>
		</td>
		<td><?php echo h($pinning['Pinning']['lat_long']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $pinning['Pinning']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $pinning['Pinning']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $pinning['Pinning']['id']), null, __('Are you sure you want to delete # %s?', $pinning['Pinning']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	

	
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Pinning'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Food Trucks'), array('controller' => 'food_trucks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Food Truck'), array('controller' => 'food_trucks', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Qualityes'), array('controller' => 'qualities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Quality'), array('controller' => 'qualities', 'action' => 'add')); ?> </li>
	</ul>
</div>
