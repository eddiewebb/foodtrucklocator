<div class="pinnings form">
<?php echo $this->Form->create('Pinning');?>
	<fieldset>
		<legend><?php echo __('Edit Pinning'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('food_truck_id');
		echo $this->Form->input('quality_id');
		echo $this->Form->input('lat_long');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Pinning.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Pinning.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Pinnings'), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Food Trucks'), array('controller' => 'food_trucks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Food Truck'), array('controller' => 'food_trucks', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Qualityes'), array('controller' => 'qualities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Quality'), array('controller' => 'qualities', 'action' => 'add')); ?> </li>
	</ul>
</div>
