<div class="pinnings view">
<h2><?php  echo __('Pinning');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($pinning['Pinning']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Food Truck'); ?></dt>
		<dd>
			<?php echo $this->Html->link($pinning['FoodTruck']['name'], array('controller' => 'food_trucks', 'action' => 'view', $pinning['FoodTruck']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($pinning['User']['username'], array('controller' => 'users', 'action' => 'view', $pinning['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($pinning['Pinning']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quality'); ?></dt>
		<dd>
			<?php echo $this->Html->link($pinning['Quality']['title'], array('controller' => 'qualities', 'action' => 'view', $pinning['Quality']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Point'); ?></dt>
		<dd>
			<?php echo h($pinning['Pinning']['point']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Pinning'), array('action' => 'edit', $pinning['Pinning']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Pinning'), array('action' => 'delete', $pinning['Pinning']['id']), null, __('Are you sure you want to delete # %s?', $pinning['Pinning']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Pinnings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pinning'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Food Trucks'), array('controller' => 'food_trucks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Food Truck'), array('controller' => 'food_trucks', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Qualityes'), array('controller' => 'qualities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Quality'), array('controller' => 'qualities', 'action' => 'add')); ?> </li>
	</ul>
</div>
