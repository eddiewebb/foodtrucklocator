<div class="foodTrucks view">
<h2><?php  echo __('Food Truck');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($foodTruck['FoodTruck']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($foodTruck['FoodTruck']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($foodTruck['FoodTruck']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($foodTruck['FoodTruck']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Food Truck'), array('action' => 'edit', $foodTruck['FoodTruck']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Food Truck'), array('action' => 'delete', $foodTruck['FoodTruck']['id']), null, __('Are you sure you want to delete # %s?', $foodTruck['FoodTruck']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Food Trucks'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Food Truck'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Pinnings'), array('controller' => 'pinnings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pinning'), array('controller' => 'pinnings', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Pinnings');?></h3>
	<?php if (!empty($foodTruck['Pinning'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Food Truck Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Quality Id'); ?></th>
		<th><?php echo __('Point'); ?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($foodTruck['Pinning'] as $pinning): ?>
		<tr>
			<td><?php echo $pinning['id'];?></td>
			<td><?php echo $pinning['food_truck_id'];?></td>
			<td><?php echo $pinning['user_id'];?></td>
			<td><?php echo $pinning['modified'];?></td>
			<td><?php echo $pinning['quality_id'];?></td>
			<td><?php echo $pinning['point'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'pinnings', 'action' => 'view', $pinning['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'pinnings', 'action' => 'edit', $pinning['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'pinnings', 'action' => 'delete', $pinning['id']), null, __('Are you sure you want to delete # %s?', $pinning['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Pinning'), array('controller' => 'pinnings', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
