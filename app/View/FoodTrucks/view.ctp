<div class="foodTrucks view">
<h2><?php  echo __('Food Truck');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($foodTruck['FoodTruck']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($foodTruck['FoodTruck']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($foodTruck['FoodTruck']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($foodTruck['FoodTruck']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Menu'), array('action' => 'menu', $foodTruck['FoodTruck']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Other Food Trucks'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Food Near Me'), array('controller' => 'pinnings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Update Location'), array('controller' => 'pinnings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('Re-Rank Pinnings'), array('controller' => 'foodTrucks', 'action' => 'rankPreviousPinnings',$foodTruck['FoodTruck']['id'])); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Pinnings');?></h3>
	

<div id="map_canvas" style="width:600px; height:300px"></div>

<?php


function zoomLevel ($distance){
    $zoom=1;
    $ee = 21638.8;
    $zoom = round(log($ee/$distance)/log(2)+1);
    // to avoid exeptions
    if ($zoom>21) $zoom=21;
    if ($zoom<1) $zoom =1;

    return $zoom;
}

?>



<script type="text/javascript">
	function initialize() {
		
	

		
		
			var myOptions = {
			  zoom: 10,
			  center: new google.maps.LatLng(<?php echo $foodTruck['Pinning'][0]['lat_long']; ?>),
			  mapTypeId: google.maps.MapTypeId.ROADMAP
			};

		   var map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
			
			// Marking targets now Sir...
			<?php 
			foreach ($foodTruck['Pinning'] as $pinning):  	?>
					var image = new google.maps.MarkerImage(
							'<?php echo ($this->webroot."/img/".$pinning['Quality']['image']); ?>'
						);
					
			
				   marker = new google.maps.Marker({
						animation: google.maps.Animation.DROP,	
						position: new google.maps.LatLng(<?php echo ($pinning['lat_long']); ?>),
						title:'<?php echo $pinning['Quality']['title'] . "-" . $pinning['modified']; ?>',
						icon: image
					});
					// To add the marker to the map, call setMap();
					marker.setMap(map);		
			<?php endforeach; ?>	
		
		
	}
	
	
		
	
		
</script>


</div>
