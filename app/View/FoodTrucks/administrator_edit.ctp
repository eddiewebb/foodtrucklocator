<div class="foodTrucks form">
<?php echo $this->Form->create('FoodTruck');?>
	<fieldset>
		<legend><?php echo __('Administrator Edit Food Truck'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('description');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('FoodTruck.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('FoodTruck.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Food Trucks'), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Pinnings'), array('controller' => 'pinnings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pinning'), array('controller' => 'pinnings', 'action' => 'add')); ?> </li>
	</ul>
</div>
