<div class="statuses view">
<h2><?php  echo __('Quality');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($status['Quality']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($status['Quality']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Accurate'); ?></dt>
		<dd>
			<?php echo h($status['Quality']['is_accurate']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Quality'), array('action' => 'edit', $status['Quality']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Quality'), array('action' => 'delete', $status['Quality']['id']), null, __('Are you sure you want to delete # %s?', $status['Quality']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Qualityes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Quality'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Pinnings'), array('controller' => 'pinnings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pinning'), array('controller' => 'pinnings', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Pinnings');?></h3>
	<?php if (!empty($status['Pinning'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Food Truck Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Quality Id'); ?></th>
		<th><?php echo __('Point'); ?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($status['Pinning'] as $pinning): ?>
		<tr>
			<td><?php echo $pinning['id'];?></td>
			<td><?php echo $pinning['food_truck_id'];?></td>
			<td><?php echo $pinning['user_id'];?></td>
			<td><?php echo $pinning['modified'];?></td>
			<td><?php echo $pinning['status_id'];?></td>
			<td><?php echo $pinning['point'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'pinnings', 'action' => 'view', $pinning['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'pinnings', 'action' => 'edit', $pinning['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'pinnings', 'action' => 'delete', $pinning['id']), null, __('Are you sure you want to delete # %s?', $pinning['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Pinning'), array('controller' => 'pinnings', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
