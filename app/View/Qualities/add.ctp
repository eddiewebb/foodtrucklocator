<div class="statuses form">
<?php echo $this->Form->create('Quality');?>
	<fieldset>
		<legend><?php echo __('Add Quality'); ?></legend>
	<?php
		echo $this->Form->input('title');
		echo $this->Form->input('is_accurate');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Qualityes'), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Pinnings'), array('controller' => 'pinnings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pinning'), array('controller' => 'pinnings', 'action' => 'add')); ?> </li>
	</ul>
</div>
