<?php
App::uses('AppModel', 'Model');
/**
 * Pinning Model
 *
 * @property FoodTruck $FoodTruck
 * @property User $User
 * @property Quality $Quality
 */
class Pinning extends AppModel {
/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'dev';
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'food_truck_id';
	
	
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'food_truck_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'status_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'FoodTruck' => array(
			'className' => 'FoodTruck',
			'foreignKey' => 'food_truck_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Quality' => array(
			'className' => 'Quality',
			'foreignKey' => 'quality_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	
	function beforeSave() {
		$db = $this->getDataSource(); 
		$latLong = explode(",",$this->data['Pinning']['lat_long']);
		$lat = floatval(trim($latLong[0]));
		$lon = floatval(trim($latLong[1]));
		//$obj = new stdClass();
		//$obj->value = "PointFromText('POINT($lat $lon)')";
		$obj->type = "expression";
		$this->data['Pinning']['gps_point'] = $db->expression("PointFromText('POINT($lat $lon)')");
		if($this->data['Pinning']['modified']=='0000-00-00 00:00:00'){
				$this->data['Pinning']['modified'] = date('Y-m-d H:i:s') ;
		}
		//die("<pre>".print_r($this->data)."</pre>");
		return true;
	}
	
	
	function withinRadiusOfCentroid($radiusInMiles,$centroidLatLong){
		$latLongArr = explode(",",$centroidLatLong);
		$db = $this->getDataSource();
		return $db->fetchAll(
			'
				SELECT Pinning.*, User.*, Quality.*,FoodTruck.*
				FROM  ftl_food_trucks FoodTruck 

				JOIN   ftl_pinnings Pinning on Pinning.food_truck_id = FoodTruck.id

				JOIN ftl_users User on Pinning.user_id = User.id
								 
									
				JOIN ftl_qualities Quality on Quality.id  = Pinning.quality_id

							
								
				left outer JOIN ftl_pinnings p2 on (p2.food_truck_id = FoodTruck.id and p2.zorder > Pinning.zorder )
								
				WHERE 
					`p2`.`id` is null
				AND


					MBRContains
						(
							LineString
							(
								Point
									 (
									 :lat + :dist/ 111.1,
									:lon + :dist/ ( 111.1 / COS(RADIANS(:lat)))
									 ),
								Point    (
									 :lat - :dist/ 111.1,
									 :lon - :dist/ ( 111.1 / COS(RADIANS(:lat)))
									 )
							),
							Pinning.gps_point
						) 
			
			',
			array(
				'lat'=>trim($latLongArr[0]),
				'lon'=>trim($latLongArr[1]),
				'dist' => $radiusInMiles
				)
		);
	}
	
}
