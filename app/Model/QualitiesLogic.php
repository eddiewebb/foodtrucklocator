<?php
App::uses('AppModel', 'Model');
/**
 * QualitiesLogic Model
 *
 * @property Role $Role
 * @property Quality $Quality
 */
class QualitiesLogic extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'qualities_logic';
/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'role_id,days_old';

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Role' => array(
			'className' => 'Role',
			'foreignKey' => 'role_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Quality' => array(
			'className' => 'Quality',
			'foreignKey' => 'quality_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
